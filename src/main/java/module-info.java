module com.lpnu.zorii.credit {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires java.sql;
    requires slf4j.api;
    requires java.mail;

    opens com.lpnu.zorii.credit to javafx.fxml;
    opens com.lpnu.zorii.credit.entity to javafx.fxml;
    opens com.lpnu.zorii.credit.controller.utils to javafx.fxml;
    exports com.lpnu.zorii.credit;
    exports com.lpnu.zorii.credit.entity;
    exports com.lpnu.zorii.credit.controller;
    opens com.lpnu.zorii.credit.controller to javafx.fxml;
    exports com.lpnu.zorii.credit.controller.authorization;
    opens com.lpnu.zorii.credit.controller.authorization to javafx.fxml;
    exports com.lpnu.zorii.credit.controller.main_menu;
    opens com.lpnu.zorii.credit.controller.main_menu to javafx.fxml;
    exports com.lpnu.zorii.credit.controller.main_menu.manager;
    opens com.lpnu.zorii.credit.controller.main_menu.manager to javafx.fxml;
}