package com.lpnu.zorii.credit.dao;

import com.lpnu.zorii.credit.dao.mysql.MysqlUserDAO;
import com.lpnu.zorii.credit.entity.ActiveLoan;
import com.lpnu.zorii.credit.entity.User;
import javafx.collections.ObservableList;

import java.util.List;

public interface UserDAO {
    UserDAO actualInstance = new MysqlUserDAO();
    static UserDAO getInstance(){return actualInstance;}
    void addUser(User user);
    User getUserByLogin(String login);
    void setStatus(String status, String userLogin);
    boolean acceptableStatus(String userLogin);
    void handleOverdue(ActiveLoan activeLoan);
    boolean passwordMatchesLogin(String login, String password);
    boolean loginExists(String login);
    ObservableList<ActiveLoan> getAllActiveLoans(String login);
}
