package com.lpnu.zorii.credit.dao;

import com.lpnu.zorii.credit.dao.mysql.MysqlBankDAO;
import com.lpnu.zorii.credit.dao.mysql.MysqlLoanOfferDAO;
import com.lpnu.zorii.credit.entity.Bank;

public interface BankDAO {
    BankDAO actualInstance = new MysqlBankDAO();

    Bank getBankById(int bankId);
}
