package com.lpnu.zorii.credit.dao;

import com.lpnu.zorii.credit.dao.mysql.MysqlLoanOfferDAO;
import com.lpnu.zorii.credit.dao.mysql.MysqlUserDAO;
import com.lpnu.zorii.credit.entity.LoanOffer;
import javafx.collections.ObservableList;

import java.util.List;

public interface LoanOfferDAO {
    LoanOfferDAO actualInstance = new MysqlLoanOfferDAO();
    LoanOffer getLoanOfferById(int loanOfferId);
    public boolean loanOffersExists(int id);
    ObservableList<LoanOffer> getLoanOffersWithMinOfferSum(int minDebtAmount);
    ObservableList<LoanOffer> getLoanOffersWithMaxPercentage(int maxPercentage);
    ObservableList<LoanOffer> getLoanOffersWithBothConditions(int minDebtSum, int maxPercentage);
    ObservableList<LoanOffer> getAllLoanOffers();
}
