package com.lpnu.zorii.credit.dao;

import com.lpnu.zorii.credit.dao.mysql.MysqlActiveLoanDAO;
import com.lpnu.zorii.credit.entity.ActiveLoan;
import com.lpnu.zorii.credit.entity.LoanOffer;
import com.lpnu.zorii.credit.entity.User;

import java.util.List;

public interface ActiveLoanDAO {
    ActiveLoanDAO actualInstance = new MysqlActiveLoanDAO();
    boolean addActiveLoan(LoanOffer loanOffer);

    boolean accrueInterest(int monthsPassed, String userLogin);
    void payTheLoan(int moneyAmount, ActiveLoan activeLoan);
    public boolean activeLoanExists(int id);
    boolean extendCreditLine(ActiveLoan activeLoan);

    ActiveLoan getActiveLoanById(int activeLoanId);
    boolean deleteActiveLoanById(int activeLoanId);
}
